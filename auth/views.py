from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from kpi import views


def log_in(request):
    if request.method == 'GET':

        return render(request, 'auth/auth.html', {})

    else:
        password = request.POST['password']
        username = request.POST['username']

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)

                return redirect(reverse('kpi:profile', args={user.id}))
            else:
                # куда?
                return redirect(reverse('auth:auth'))

        return redirect(request.path)



@login_required
def log_out(request):
    logout(request)
    return redirect(reverse('auth:auth'))
