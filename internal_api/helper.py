from django.core.paginator import Paginator, EmptyPage
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from django.db import connection


class MyJsonResponse(JsonResponse):
    def __init__(self, data, encoder=DjangoJSONEncoder, safe=True, **kwargs):
        json_dumps_params = dict(ensure_ascii=False)
        super().__init__(data, encoder, safe, json_dumps_params, **kwargs)


def pagination(request, entities):
    try:
        page = int(request.GET['page'])
        number = int(request.GET['pageing_by'])
        paginator = Paginator(entities, number)
        entities = paginator.page(page)
    except KeyError:
        pass
    except EmptyPage:
        entities = []
    return entities


def recursive_update(rep, done_count):
    rep.owner.done_count += done_count
    rep.owner.save()
    cursor = connection.cursor()
    try:
        cursor.execute('''
            WITH RECURSIVE r AS (
               SELECT id, done_count, parent_id
               FROM kpi_indicator
               WHERE id = {}

               UNION

               SELECT kpi_indicator.id, kpi_indicator.done_count, kpi_indicator.parent_id
               FROM kpi_indicator
                  JOIN r
                      ON kpi_indicator.id = r.parent_id
            )
            update kpi_indicator set done_count = done_count + {} where id in (select r.id from r)'''.format(
            rep.owner.indicator.id, done_count))
    finally:
        cursor.close()
