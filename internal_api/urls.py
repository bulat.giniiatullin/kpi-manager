from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^users/update$', views.users, name='update_users'),

    url(r'^indicators$', views.indicators, name='indicators'),
    url(r'^indicators/undistributed$', views.undistributed_indicators, name='undistributed_indicators'),
    url(r'^indicators/(?P<indicator_id>\d+)/distribution_list$', views.distribution_list, name='distribution_list'),
    url(r'^indicators/self_distribute', views.self_distribute, name='self-distribute'),

    url(r'^reports$', views.reports, name='reports'),
    url(r'^reports/(?P<report_id>\d+)$', views.report, name='report'),
    url(r'^reports/under_consideration$', views.reports_under_consideration, name='reports_under_consideration'),

    url(r'^sets$', views.sets, name='sets'),
    url(r'^sets/(?P<set_id>\d+)/indicators$', views.indicators_in_set, name='indicators_in_set'),
    url(r'^sets/(?P<set_id>\d+)/generate_report_file$', views.generate_report_file, name='generate_report_file'),
    url(r'^sets/(?P<set_id>\d+)$', views.set_info, name='set_info'),

    url(r'^departments/under_control', views.departments_under_control, name='departments_under_control'),
]