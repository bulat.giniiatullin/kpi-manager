from django.apps import AppConfig


class InternalApiConfig(AppConfig):
    name = 'internal_api'
