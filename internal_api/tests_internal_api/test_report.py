from unittest import skip

from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory, Client
from unittest.mock import *
# from unittest import *
# Create your tests here.
from kpi.models import *
from internal_api.views import report


class TestReportModel(TestCase):
    # TODO mock integration
    allow_database_queries = True

    @classmethod
    def setUpTestData(cls):
        cls.report_id = 99
        cls.factory = RequestFactory()

        cls.user_with_access = User.objects.create(username="user1", password="password")
        cls.employee = Employee.objects.create(user=cls.user_with_access, fio="Абрамский Михаил Михайлович",
                                               date_of_birth="2012-12-12")
        cls.indicator = Indicator.objects.create(context_id=10, set_id=10)
        cls.ife = IndicatorsForEmployees.objects.create(employee=cls.employee, indicator=cls.indicator)
        cls.report = Report.objects.create(owner=cls.ife)
        cls.report.id = cls.report_id
        cls.report.save()

        cls.user_with_no_access = User.objects.create(username="user_with_no_access", password="password")
        cls.employee_with_no_access = Employee.objects.create(user=cls.user_with_no_access, fio="Добрынин Елисей",
                                                              date_of_birth="2018-12-12")

        # rector_e = Employee.objects.create(user=rector, fio="Гафуров",date_of_birth="12.12.2012")
        #
        # kfu_head_department = Department.objects.create(name="КФУ")
        # itis_department = Department.objects.create(name="ИТИС",parent=kfu_head_department)
        #
        # set = Set.objects.create(name="2018")
        #
        # SetsInDepartments.objects.create(department=kfu_head_department,set=set)
        #
        # Report.objects.create(name="Отчет о проведении практики")
        #
        # EmployeesInDepartments.objects.create(employee=rector_e,department=kfu_head_department, is_director=True)
        # EmployeesInDepartments.objects.create(employee=abra_e,department=itis_department)
        #
        # good_students = Indicator.objects.create(set=set,description="Количество ")

    def test_report_anonymous_user(self):
        self.report_id = 99
        request = RequestFactory().get('/reports/{}'.format(self.report_id))
        request.user = AnonymousUser()
        response = report(request, self.report_id)
        self.assertEqual(response.status_code, 302)
        # self.assertRedirects(response, expected_url='/login')

    def test_report_employee_with_access(self):
        request = self.factory.get('/reports/{}'.format(self.report_id))

        request.user = self.user_with_access
        request.user.employee = self.employee
        indicator_for_employees = self.ife

        rep = self.report
        rep.owner = indicator_for_employees
        response = report(request, self.report_id)

        self.assertEqual(response.status_code, 200)

    def test_report_employee_with_no_access(self):
        request = self.factory.get('/reports/{}'.format(self.report_id))

        request.user = self.user_with_no_access
        request.user.employee = self.employee_with_no_access
        response = report(request, self.report_id)
        self.assertEqual(response.status_code, 403)

    @skip("Not fixed yet")
    def test_report_anon_redirect_to_login(self):
        c = Client()
        response = c.get('/reports/100')
        self.assertRedirects(response, expected_url='/login')
        self.assertEqual(response.status_code, 302)
