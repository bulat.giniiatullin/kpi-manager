from datetime import date

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import F
from django.http import HttpResponseForbidden, HttpResponse, HttpResponseBadRequest, \
    QueryDict
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

import kpi.views
from internal_api.helper import MyJsonResponse, pagination, recursive_update
from kpi.helper import validate_user_data
from kpi.models import *


def indicators(request):
    indicators_for_user = request \
        .user \
        .employee \
        .indicatorsforemployees_set \
        .select_related('indicator__context') \
        .select_related('indicator__set__set') \

    if 'name' in request.GET and request.GET['name'] != '':
        indicators_for_user = indicators_for_user \
            .filter(indicator__context__name=request.GET['name'])
    if 'date_from' in request.GET:
        indicators_for_user = indicators_for_user \
            .filter(indicator__set__set__start_date__range__gte=request.GET['date_from'])
    if 'date_to' in request.GET:
        indicators_for_user = indicators_for_user.filter(
            indicator__set__set__end_date__lte=request.GET['date_to'])
    if 'set' in request.GET:
        indicators_for_user = indicators_for_user \
            .filter(indicator__set__set__id=request.GET['set'])
    if 'completed' in request.GET:
        if request.GET['completed'] == 'completed':
            indicators_for_user = indicators_for_user \
                .filter(count=F('done_count'))
        elif request.GET['completed'] == 'uncompleted':
            indicators_for_user = indicators_for_user \
                .filter(count__gt=F('done_count'))
    indicators_for_user = pagination(request, indicators_for_user)
    return MyJsonResponse({'indicators': [indicator.serialize() for indicator in indicators_for_user]})


def reports(request):
    #  TODO ограничить несанкционированный доступ
    if not Indicator.objects.filter(id=int(request.GET['indicator']),
                                    set__department__employees_in_departments__is_director=True,
                                    set__department__employees_in_departments__employee__user=request.user
                                    ).exists():
        return HttpResponseForbidden()

    indicator = get_object_or_404(Indicator, pk=int(request.GET['indicator']))
    report_list = Report.objects.filter(owner__indicator=indicator)
    if 'user' in request.GET:
        if request.GET['user'] == 'current':
            user_id = request.user.id
        else:
            user_id = int(request.GET['user'])
        report_list = report_list.filter(owner__employee__user__id=user_id)
    report_list = pagination(request, report_list)
    return MyJsonResponse({'report_list': [rep.short_serialize() for rep in report_list]})


@transaction.atomic
@login_required()
def report(request, report_id):
    employee_id = request.user.employee.id
    if not Report.objects.filter(id=report_id, owner__employee__id=employee_id).select_related().exists():
        return HttpResponseForbidden()
    rep = get_object_or_404(Report, pk=report_id)
    if request.method == 'GET':
        return MyJsonResponse(rep.serialize())
    elif request.method == 'POST':
        #  TODO ещё больше ограничить несанкционированный доступ
        if 'decision' in request.POST and request.POST['decision'] in ('accept', 'reject'):
            if request.POST['decision'] == 'accept':
                if rep.status != 'A':
                    rep.status = 'A'
                    recursive_update(rep, rep.done_count)
            elif request.POST['decision'] == 'reject':
                if rep.status == 'A':
                    recursive_update(rep, -rep.done_count)
                rep.status = 'R'
            rep.save()
            return HttpResponse()
        else:
            return HttpResponseBadRequest()


@login_required
def undistributed_indicators(request):
    department_list = request\
        .user\
        .employee\
        .departments\
        .filter(employees_in_departments__is_director=True)
    indicator_list = Indicator\
        .objects\
        .filter(set__department__id__in=department_list.values_list('id', flat=True))\
        .annotate(sum_dep=Coalesce(Sum('children__count'), 0))\
        .annotate(sum_emp=Coalesce(Sum('indicators_for_employees__count'), 0))\
        .annotate(sum=F('sum_dep')+F('sum_emp'))\
        .filter(count__gt=F('sum'))\
        .select_related('context')
    indicator_list = pagination(request, indicator_list)
    return MyJsonResponse({'indicators': [indicator.serialize() for indicator in indicator_list]})


def users(request):
    if request.method == 'POST':
        data = request.POST
        is_valid, error_code = validate_user_data(data)
        if is_valid:
            user = request.user
            if 'login' in data and data['login'] != '':
                user.username = data['login']
            if 'password' in data and data['password'] != '':
                user.set_password(data['password'])
            if 'email' in data and data['email'] != '':
                user.email = data['email']
            user.save()
            update_session_auth_hash(request, user)
            employee = user.employee
            if 'name' in data and data['name'] != '':
                employee.fio = data['name']
            if 'birth-date' in data and data['birth-date'] != '':
                employee.date_of_birth = data['birth-date']
            if 'user-photo' in request.FILES:
                employee.photo = request.FILES['user-photo']
            employee.save()
            return HttpResponse(status=201)
        else:
            response = HttpResponseBadRequest()
            response['error_code'] = error_code
            return response

@login_required()
def distribution_list(request, indicator_id):
    if not Indicator.objects.filter(id=indicator_id,
                                    set__department__employees_in_departments__is_director=True):
        return HttpResponseForbidden()

    indicator = get_object_or_404(Indicator
                                  .objects
                                  .prefetch_related('children__set__department')
                                  .prefetch_related('indicators_for_employees__employee'), pk=indicator_id)
    response_list = indicator.distribution_list()
    response_list = pagination(request, response_list)
    return MyJsonResponse(response_list, safe=False)


@transaction.atomic
def sets(request):
    if request.method == 'POST':
        if 'name' in request.POST and request.POST['name'] != '' and \
            'date_from' in request.POST and 'date_to' in request.POST and \
                'department_id' in request.POST and request.POST['department_id'] != '':
            department = get_object_or_404(Department, pk=int(request.POST['department_id']))
            if not department\
                    .employees_in_departments\
                    .filter(employee=request.user.employee, is_director=True)\
                    .exists():
                return HttpResponseForbidden()
            new_set = Set.objects.create(
                name=request.POST['name'],
                start_date=request.POST['date_from'],
                end_date=request.POST['date_to'],
                parent_department=department
            )
            SetsInDepartments.objects.create(
                department=department,
                set=new_set
            )
            return HttpResponse()
        else:
            response = HttpResponseBadRequest()
            response['error'] = 'Incorrect data'
            return response
    elif request.method == 'GET':
        set_list = SetsInDepartments\
            .objects\
            .filter(department__in=request
                    .user
                    .employee
                    .departments
                    .filter(employees_in_departments__is_director=True))\
            .select_related('set')\
            .select_related('department')
        if 'name' in request.GET and request.GET['name'] != '':
            set_list = set_list.filter(set__name=request.GET['name'])
        if 'completed' in request.GET:
            if request.GET['completed'] == 'completed':
                set_list = set_list.filter(set__end_date__lt=date.today())
            elif request.GET['completed'] == 'uncompleted':
                set_list = set_list.filter(set__end_date__gte=date.today())
        if 'department_id' in request.GET and request.GET['department_id'] != '':
            set_list = set_list.filter(department__id=int(request.GET['department_id']))
        set_list = pagination(request, set_list)
        return MyJsonResponse({'set_list': [s.serialize() for s in set_list]})


def indicators_in_set(request, set_id):
    dep_set = get_object_or_404(SetsInDepartments, pk=set_id)
    if not request.user.employee.employees_in_departments \
            .filter(is_director=True) \
            .filter(department__all_sets__id=set_id).exists():
        return HttpResponseForbidden()
    indicator_list = dep_set \
        .indicator_set \
        .select_related('context') \
        .prefetch_related('indicators_for_employees__employee')
    if 'name' in request.GET:
        if request.GET['name'] != '':
            indicator_list = indicator_list.filter(context__name=request.GET['name'])
    if 'completed' in request.GET:
        if request.GET['completed'] == 'completed':
            indicator_list = indicator_list.filter(count=F('done_count'))
        elif request.GET['completed'] == 'uncompleted':
            indicator_list = indicator_list.filter(count__gt=F('done_count'))
    if 'employee_name' in request.GET:
        if request.GET['employee_name'] != '':
            indicator_list = indicator_list.filter(indicators_for_employees__employee__fio=request.GET['employee_name'])
    indicator_list = pagination(request, indicator_list)
    return MyJsonResponse({'indicator_list': [indicator.serialize() for indicator in indicator_list]})


def generate_report_file(request, set_id):
    pass


def departments_under_control(request):
    departments = request.user.employee.departments.filter(employees_in_departments__is_director=True)
    departments_list = []
    for department in departments:
        departments_list.append({'id': department.id, 'name': department.name})
    return MyJsonResponse({'departments': departments_list})


@csrf_exempt
def self_distribute(request):
    if request.method == 'POST':
        indicator_id = request.POST['indicator_id']
        todo_count = request.POST['todo_count']
        employee_id = request.user.employee.id
        query_dict = QueryDict(mutable=True)
        query_dict.update({'emp_id_0': employee_id, 'emp_count_0': todo_count})
        request.POST = query_dict
        kpi.views.distribute_indicator(request, indicator_id)


@login_required()
def set_info(request, set_id):
    current_set = get_object_or_404(SetsInDepartments, pk=set_id)
    if current_set\
            .department\
            .employees_in_departments\
            .filter(employee=request.user.employee, is_director=True)\
            .exists():
        aggregation_result = current_set\
            .indicator_set\
            .aggregate(total_count=Coalesce(Sum('count'), 0), total_done_count=Coalesce(Sum('done_count'), 0))
        return MyJsonResponse({'set': {'id': set_id,
                                       'total_count': aggregation_result['total_count'],
                                       'total_done_count': aggregation_result['total_done_count']}})
    else:
        return HttpResponseForbidden()


@login_required()
def reports_under_consideration(request):
    report_list = Report\
        .objects\
        .filter(status='C', owner__indicator__set__department__employees_in_departments__in=request
                .user.employee
                .employees_in_departments
                .filter(is_director=True))
    report_list = pagination(request, report_list)
    return MyJsonResponse({'report_list': [rep.serialize() for rep in report_list]})

