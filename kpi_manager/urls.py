from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from internal_api import urls as internal_api_urls
from kpi_manager import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('auth.urls', namespace='auth'), name='auth'),
    url(r'', include('kpi.urls', namespace='kpi')),
    url(r'^internal_api/', include(internal_api_urls, namespace='internal_api'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
