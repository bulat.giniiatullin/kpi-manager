import datetime

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import Sum, F
from django.http import HttpResponseForbidden, HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.db.models.functions import Coalesce
from django.http import HttpResponseForbidden, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import TemplateView

from kpi.helper import is_director, can_watch_page, redirect_with_params
from kpi.models import *


@login_required
def profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    employee = user.employee
    return render(request, 'kpi/profile.html', {'employee': employee})


@login_required
@can_watch_page
def stats(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    employee = user.employee
    users_tasks = employee.indicators.all()
    return render(request, 'kpi/stats.html', {'tasks': users_tasks})


@login_required
@is_director
def employees(request):
    user = request.user
    employee = user.employee
    departments = employee.departments.filter(is_director=True)
    return render(request, 'kpi/employees.html', {'current_employee': employee, 'departments': departments})


@login_required
def tasks(request):
    employee = request.user.employee
    return render(request, 'kpi/tasks.html', {'tasks': employee.indicators.all()})


@login_required
@is_director
def employees_tasks(request):
    user = request.user
    director = user.employee
    departments = director.departments.filter(is_director=True)
    departments_tasks = []
    subordinates_tasks = []
    for department in departments:
        for task in department.tasks.all():
            if not task.is_distributed():
                departments_tasks.append(task)
            for child_task in task.children.all():
                subordinates_tasks.append(child_task)
    return render(request, 'kpi/employees_tasks.html',
                  {'departments_tasks': departments_tasks, 'subordinates_tasks': subordinates_tasks})


@login_required
def report(request, report_id):
    rep = get_object_or_404(Report, pk=report_id)
    if request.user.employee.can_watch_task(rep.owner):
        return render(request, 'kpi/report.html', {'rep': rep})
    else:
        return HttpResponseForbidden()


@login_required
@can_watch_page
def reports_list(request, task_id):
    task = get_object_or_404(Indicator, pk=task_id)
    return render(request, 'kpi/reports-list.html', {'task': task})


@login_required
@is_director
def update_task(request, task_id):
    pass
    # task = get_object_or_404(Indicator, pk=task_id)
    # error = ''
    # if task.is_distributed():
    #     return redirect(reverse(employees_tasks))
    # if request.method == "POST":
    #     dep_id = request.POST['department']
    #     emp_id = request.POST['employee']
    #     new_task = Task(parent=task, description=request.POST['description'],
    #                     parent_id=task.id, context=task.context, count=int(request.POST['count']),
    #                     date=request.POST['date'])
    #     if int(request.POST['count']) + task.get_distributed_count() > task.count:
    #         error = 'Вы не можете распределить больше {0} заданий'.format(task.get_not_distributed_count())
    #     elif dep_id is '' and emp_id is not '':
    #         new_task.employee = Employee.objects.get(pk=emp_id)
    #         new_task.save()
    #         return HttpResponseRedirect(reverse('employees_tasks'))
    #     elif dep_id is not '' and emp_id is '':
    #         new_task.department = Department.objects.get(pk=dep_id)
    #         new_task.save()
    #         return HttpResponseRedirect(reverse('employees_tasks'))
    #     else:
    #         error = 'Заполните ОДНО из полей: подразделение или сотрудник'
    # department = task.department
    # emp_list = Employee.objects.filter(departments_e=department)
    # dep_list = Department.objects.filter(parent=department)
    # return render(request, 'kpi/add-task.html', {'emp_list': emp_list, 'dep_list': dep_list,
    #                                                  'err': error, 'task': task})


@login_required
def create_task(request):
    pass
    # employee = request.user.employee
    # if request.method == 'POST':
    #     count = request.POST['count']
    #     date = request.POST['date']
    #     #contest_data = название задачи
    #     context_data = request.POST['name']
    #     description = request.POST['description']
    #
    #     context = TaskContext.objects.create(
    #         name=context_data,
    #     )
    #
    #     dep_id = request.POST['department']
    #     department = Department.objects.get(id=dep_id)
    #
    #     new_task = Task.objects.create(
    #         description=description,
    #         context=context,
    #         count=count,
    #         date=date,
    #         department=department,
    #     )
    #
    #     return redirect('employees_tasks')
    # else:
    #     return render(request, 'kpi/create-task.html', {"deps": employee.departments_d.all()})


@login_required
def execute_task(request, task_id):
    pass
    # task = get_object_or_404(Task, pk=task_id)
    # if request.method == 'POST':
    #     report_name = request.POST['report-name']
    #     description = request.POST['to-do']
    #     textarea = request.POST['textarea']
    #     if int(description) + task.get_done_count() > task.count:
    #         error = 'Вы не можете выполнить сверх плана'
    #         return render(request, 'kpi/execute.html', {"task": task, 'error': error})
    #     else:
    #         report = Report.objects.create(
    #             owner=task,
    #             done_count=description,
    #             name=report_name,
    #             description=textarea,
    #         )
    #         try:
    #             File.objects.create(
    #               file=request.FILES['file'],
    #               owner=report,
    #             )
    #         except MultiValueDictKeyError:
    #             pass
    #
    #         return redirect('tasks')
    # else:
    #     return render(request, 'kpi/execute.html', {"task": task})


def department_stats(request, dep_id):
    pass
    # if request.user.employee.departments_d.filter(pk=dep_id).exists():
    #     department = get_object_or_404(Department, pk=dep_id)
    #     departments = department.children.all()
    #     return render(request, 'kpi/employees.html', {'departments': departments})
    # else:
    #     return HttpResponseForbidden()


def redirect_to_login_page(request):
    return redirect(reverse('auth:auth'))



def indicators(request):
    indicators_for_user = request \
        .user \
        .employee \
        .indicators \
        .select_related('set__set')
    available_sets = indicators_for_user \
        .values('set__set__id', 'set__set__name').distinct()
    return render(request, 'kpi/indicators.html', {'sets': available_sets})


def indicators_in_set(request, set_id):
    dep_set = get_object_or_404(SetsInDepartments.objects.select_related('set'), pk=set_id)
    if not request.user.employee.employees_in_departments\
            .filter(is_director=True)\
            .filter(department__all_sets__id=set_id).exists():
        return HttpResponseForbidden()
    return render(request, 'kpi/indicators_in_set.html', {'dep_set': dep_set, 'set_id': set_id})


def profile_settings(request):
    return render(request, 'kpi/settings.html', {})


@transaction.atomic
def send_report(request, indicator_id):
    indicator_for_user = get_object_or_404(IndicatorsForEmployees
                                           .objects
                                           .select_related('indicator__set__set'), pk=indicator_id)
    if request.method == 'GET':
        context = {'indicator': indicator_for_user}
        if 'error' in request.GET:
            context.update({'error': request.GET['error']})
        return render(request, 'kpi/send_report.html', context)
    elif request.method == 'POST':
        try:
            done_count = int(request.POST['done_count'])
            if done_count + indicator_for_user.done_count > indicator_for_user.count:
                return redirect_with_params('kpi:send_report', indicator_id,
                                            error='Вы не можете выполнить сверх палана')
            if done_count < 1:
                return redirect_with_params('kpi:send_report', indicator_id, error='Нельзя выполнить меньше 1')
        except (ValueError, KeyError) as e:
            return redirect_with_params('kpi:send_report', indicator_id, error='Корректно заполнить поле "Сделано"')
        rep = Report.objects.create(
            owner=indicator_for_user,
            name=request.POST['name'],
            done_count=int(request.POST['done_count']),
            description=request.POST['description']
        )
        try:
            rep.file = request.FILES['file']
            rep.save()
        except MultiValueDictKeyError:
            pass
        return redirect_with_params('kpi:indicators', message='Отчёт успешно создан')


class UndistributedIndicators(TemplateView):
    template_name = 'kpi/undistributed_indicators.html'


class IndicatorsSets(TemplateView):
    template_name = 'kpi/indicators_sets.html'


@transaction.atomic
def create_indicator(request):
    if request.method == 'POST':
        is_valid = Department.objects.filter(id=request.POST['department_id']).exists() \
                   and SetsInDepartments.objects.filter(id=request.POST['set_id']).exists() \
                   and str.isdigit(request.POST['count'])
        if is_valid:
            indicator = Indicator()
            indicator.context, created = IndicatorContext.objects.get_or_create(name=request.POST['context'],
                                                                                parent_department_id=request.POST['department_id'])
            indicator.description = request.POST['description']
            indicator.set = SetsInDepartments.objects.get(pk=request.POST['set_id'])
            indicator.count = request.POST['count']
            indicator.save()
            return redirect(reverse('kpi:undistributed_indicators'))
        else:
            return HttpResponseBadRequest()
    elif request.method == 'GET':
        departments = request.user.employee.departments.filter(employees_in_departments__is_director=True)
        indicator_contexts = IndicatorContext.objects.filter(parent_department__in=departments)
        return render(request, 'kpi/create_indicator.html', {'departments': departments,
                                                             'indicator_contexts': indicator_contexts})


@transaction.atomic
def distribute_indicator(request, indicator_id):
    indicator = get_object_or_404(Indicator.objects
                                  .select_related('context')
                                  .select_related('set__set')
                                  .prefetch_related('set__department__children')
                                  .prefetch_related('set__department__employees'), pk=indicator_id)
    if indicator \
            .set \
            .department \
            .employees_in_departments \
            .filter(is_director=True, employee=request.user.employee) \
            .exists():
        if request.method == 'GET':
            return render(request, 'kpi/distribute_indicator.html', {'indicator': indicator})
        elif request.method == 'POST':
            rest = indicator.rest()
            i = 0
            while 'dep_id_{}'.format(i) in request.POST:
                if request.POST['dep_id_{}'.format(i)] == '' or request.POST['dep_count_{}'.format(i)] == '':
                    break
                rest -= int(request.POST['dep_count_{}'.format(i)])
                if rest < 0:
                    return redirect_with_params('kpi:distribute_indicator',
                                                error='Нельзя распределять больше допустимого')
                child_set, created = SetsInDepartments \
                    .objects \
                    .get_or_create(department=Department.objects.get(pk=int(request.POST['dep_id_{}'.format(i)])),
                                   set=indicator.set.set)
                ind, created = Indicator.objects.get_or_create(
                    parent=indicator,
                    set=child_set,
                    context=indicator.context
                )
                ind.count += int(request.POST['dep_count_{}'.format(i)])
                ind.save()
                i += 1
            i = 0
            while 'emp_id_{}'.format(i) in request.POST:
                if request.POST['emp_id_{}'.format(i)] == '' or request.POST['emp_count_{}'.format(i)] == '':
                    break
                rest -= int(request.POST['emp_count_{}'.format(i)])
                if rest < 0:
                    return redirect_with_params('kpi:distribute_indicator',
                                                error='Нельзя распределять больше допустимого')
                ind, created = IndicatorsForEmployees.objects.get_or_create(
                    indicator=indicator,
                    employee=Employee.objects.get(pk=int(request.POST['emp_id_{}'.format(i)])),
                )
                ind.count += int(request.POST['emp_count_{}'.format(i)])
                ind.save()
                i += 1
            return redirect('kpi:undistributed_indicators')

    else:
        return HttpResponseForbidden()


def reports_for_indicator(request, indicator_id):
    return render(request, 'kpi/reports_list_page.html', {})


def reports_under_consideration(request):
    return render(request, 'kpi/reports_under_consideration.html', {})
