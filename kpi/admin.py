from django.contrib import admin
from kpi.models import *

admin.site.register(Employee)
admin.site.register(Department)
admin.site.register(EmployeesInDepartments)
admin.site.register(Set)
admin.site.register(SetsInDepartments)
admin.site.register(IndicatorContext)
admin.site.register(Indicator)
admin.site.register(IndicatorsForEmployees)
admin.site.register(Report)
