from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce


class Department(models.Model):
    parent = models.ForeignKey('self', related_name='children', blank=True, null=True)
    name = models.CharField(max_length=200, verbose_name='Название')
    address = models.CharField(max_length=80, blank=True, null=True, verbose_name='Адрес')
    employees = models.ManyToManyField('Employee',
                                       through='EmployeesInDepartments',
                                       verbose_name='Сотрудники',
                                       related_name='departments')

    def __str__(self):
        return self.name


class EmployeesInDepartments(models.Model):
    employee = models.ForeignKey('Employee', related_name='employees_in_departments')
    department = models.ForeignKey(Department, related_name='employees_in_departments')
    is_director = models.BooleanField()

    def __str__(self):
        return 'Департамент: {}, сотрудник: {}'.format(self.department.name, self.employee.fio)


class Employee(models.Model):
    user = models.OneToOneField(User, related_name='employee')
    fio = models.CharField(max_length=50, blank=True, null=True, verbose_name='ФИО')
    position = models.CharField(max_length=100, default='')
    date_of_birth = models.DateField(verbose_name='Возраст')
    photo = models.ImageField(blank=True, null=True, verbose_name='Фото', upload_to='img/user_photo')

    def __str__(self):
        return self.fio

    def is_director(self):
        return self.departments.filter(employees_in_departments__is_director=True).exists()


class Set(models.Model):
    name = models.CharField(max_length=50)
    start_date = models.DateField()
    end_date = models.DateField()
    all_departments = models.ManyToManyField(Department, related_name='all_sets', through='SetsInDepartments')
    parent_department = models.ForeignKey(Department, related_name='created_sets')

    def __str__(self):
        return self.name


class SetsInDepartments(models.Model):
    department = models.ForeignKey(Department, related_name='all_sets_in_departments')
    set = models.ForeignKey(Set, related_name='all_sets_in_departments')

    def __str__(self):
        return '{} - {}'.format(self.department.name, self.set.name)

    def serialize(self):
        return {'id': self.id,
                'name': self.set.name,
                'start_date': self.set.start_date,
                'end_date': self.set.end_date,
                'department_name': self.department.name}


class IndicatorContext(models.Model):
    name = models.CharField(max_length=200)
    parent_department = models.ForeignKey(Department, related_name='indicator_contexts')

    def __str__(self):
        return self.name


class Indicator(models.Model):
    parent = models.ForeignKey('Indicator', related_name='children', blank=True, null=True)
    set = models.ForeignKey(SetsInDepartments)
    description = models.TextField(blank=True, null=True, verbose_name='Описание')
    context = models.ForeignKey(IndicatorContext, related_name='indicators', verbose_name='Контекст')
    employees = models.ManyToManyField(Employee, through='IndicatorsForEmployees', related_name='indicators')
    count = models.IntegerField(default=0, verbose_name='Кол-во', validators=[MinValueValidator(0)])
    done_count = models.IntegerField(default=0, verbose_name='Сделано', validators=[MinValueValidator(0)])

    def __str__(self):
        return '{0} (Департамент: {1})'.format(self.context.name, self.set.department.name)

    def distribution_list(self):
        return {'distribution_list': [child.short_serialize() for child in self.children.all()] +
                                     [ind_empl.short_serialize() for ind_empl in self.indicators_for_employees.all()]}

    def short_serialize(self):
        return {'id': self.id,
                'department_name': self.set.department.name,
                'done_count': self.done_count,
                'count': self.count}

    def serialize(self):
        return {'id': self.id,
                'name': self.context.name,
                'description': self.description,
                'done_count': self.done_count,
                'count': self.count,
                'deadline': self.set.set.end_date.strftime('%d.%m.%Y'),
                'set_name': self.set.set.name}

    def rest(self):
        return self.count \
                   - self.children.aggregate(count__sum=Coalesce(Sum('count'), 0))['count__sum'] \
                   - self.indicators_for_employees.aggregate(count__sum=Coalesce(Sum('count'), 0))['count__sum']


class IndicatorsForEmployees(models.Model):
    indicator = models.ForeignKey(Indicator, related_name='indicators_for_employees')
    employee = models.ForeignKey(Employee)
    done_count = models.IntegerField(default=0, verbose_name='Сделано', validators=[MinValueValidator(0)])
    count = models.IntegerField(default=0, verbose_name='Кол-во', validators=[MinValueValidator(0)])

    def __str__(self):
        return '{0} (Сотрудник: {1})'.format(self.indicator.context.name, self.employee.fio)

    def short_serialize(self):
        return {'id': self.id,
                'indicator_id': self.indicator.id,
                'employee_fio': self.employee.fio,
                'done_count': self.done_count,
                'count': self.count}

    def serialize(self):
        return {'id': self.id,
                'indicator_id': self.indicator.id,
                'name': self.indicator.context.name,
                'set_name': self.indicator.set.set.name,
                'description': self.indicator.description,
                'deadline': self.indicator.set.set.end_date.strftime('%d.%m.%Y'),
                'done_count': self.done_count,
                'count': self.count}

    def get_not_done_count(self):
        return self.count - self.done_count


class Report(models.Model):
    UNDER_CONSIDERATION = 'C'
    ACCEPTED = 'A'
    REJECTED = 'R'

    STATUS_CHOICES = ((UNDER_CONSIDERATION, 'На рассмотрении'),
                      (ACCEPTED, 'Принято'),
                      (REJECTED, 'Отклонено'))

    name = models.CharField(max_length=50, default='')
    owner = models.ForeignKey(IndicatorsForEmployees, related_name='reports', verbose_name='Показатель')
    done_count = models.IntegerField(default=0, verbose_name='Сделано', validators=[MinValueValidator(0)])
    description = models.TextField(verbose_name='Отчёт')
    date = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=UNDER_CONSIDERATION)
    file = models.FileField(verbose_name='Файл', upload_to='report_files/', null=True, blank=True)

    def __str__(self):
        return self.name

    def short_serialize(self):
        return {'id': self.id,
                'name': self.name,
                'employee': self.owner.employee.fio,
                'date': self.date.strftime('%d.%m.%Y'),
                'status': self.status,
                'done_count': self.done_count,
                'file': self.file.url if self.file else ''}

    def serialize(self):
        return {'id': self.id,
                'name': self.name,
                'employee': self.owner.employee.fio,
                'description': self.description,
                'date': self.date.strftime('%d.%m.%Y'),
                'status': self.status,
                'done_count': self.done_count,
                'file': self.file.url if self.file else ''}
