from django.conf.urls import url
from auth.views import log_out
from . import views

urlpatterns = [
    url(r'^profile/(?P<user_id>[0-9]+)$', views.profile, name='profile'),
    url(r'^profile/settings$', views.profile_settings, name='profile_settings'),

    url(r'^logout/', log_out, name='logout'),

    url(r'^indicators/$', views.indicators, name='indicators'),
    url(r'^indicators/send_report/(?P<indicator_id>\d+)$', views.send_report, name='send_report'),
    url(r'^indicators/undistributed$', views.UndistributedIndicators.as_view(), name='undistributed_indicators'),
    url(r'^indicators/sets$', views.IndicatorsSets.as_view(), name='indicators_sets'),
    url(r'^indicators/sets/(?P<set_id>\d+)$', views.indicators_in_set, name='indicators_in_set'),
    url(r'^indicators/create$', views.create_indicator, name='create_indicator'),
    url(r'^indicators/distribute/(?P<indicator_id>\d+)$', views.distribute_indicator, name='distribute_indicator'),
    url(r'^indicators/reports$', views.reports_under_consideration, name='reports_under_consideration'),
    url(r'^indicators/reports/(?P<indicator_id>\d+)$', views.reports_for_indicator, name='reports_for_indicator'),

    url(r'^$', views.redirect_to_login_page, name='redirect_to_login_page'),
]