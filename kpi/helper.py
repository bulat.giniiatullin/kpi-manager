import re

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponseForbidden, HttpResponse, HttpResponseRedirect


def is_director(function):
  def wrap(request, *args, **kwargs):
        if request.user.employee.is_director():
             return function(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()
  return wrap


def can_watch_page(function):
  def wrap(request, *args, **kwargs):
        if request.user.employee.can_watch_page(request.user.employee):
             return function(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()
  return wrap


def redirect_with_params(url_name, *args, **kwargs):
    from django.core.urlresolvers import reverse
    import urllib.parse
    url = reverse(url_name, args=args)
    params = urllib.parse.urlencode(kwargs)
    return HttpResponseRedirect(url + "?%s" % params)


def validate_user_data(user_form):
    if 'password' in user_form and user_form['password'] != '' and len(user_form['password']) < 8:
        return False, 'password.short'
    elif 'login' in user_form and user_form['login'] != '' and not re.fullmatch(r'[A-Za-z][A-Za-z\d_.]*', user_form['login']):
        return False, 'login.invalid'
    elif 'email' in user_form and user_form['email'] != '' and not re.fullmatch(r'[^@]+@[^@]+\.[A-Za-z\d]+', user_form['email']):
        return False, 'email.invalid'
    else:
        return True, ''
