const ON_ONE_PAGE = 10;
let current_page = 1;

$(document).ready(function () {
    reload_reports(true);

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() === $(document).height()) {
            reload_reports(false);
        }
    });
});

function reload_reports(from_first_page = false) {
    $('#loading_icon').css({'display': 'block'});

    let params = {};

    params.indicator = window.location.href.split('/').pop();

    params.pageing_by = ON_ONE_PAGE;

    if (from_first_page) current_page = 1;
    else current_page += 1;

    params.page = current_page;

    $.ajax({
        async: false,
        method: 'GET',
        url: '/internal_api/reports',
        data: params,
        dataType: 'json',
        success: function (data) {
            render_reports(data['report_list'], from_first_page);
            $('#loading_icon').css('display', 'none');
        }
    });
}

function render_reports(reports, from_first_page = false) {
    if (from_first_page) $('#reports_container').empty();
    reports.forEach(render_report);
}

function render_report(report) {
    let report_template = `<div class="row report-block" id="rep${report.id}">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6"><a id="report_modal_link_${report.id}" class="open-report-window">${report.name}</a></div>
                                  <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">${report.employee}</div>
                                  <div class="col-lg-2 col-md-2 col-sm-4 hidden-xs">${report.date}</div>
                                  <div class="col-lg-2 col-md-2 hidden-sm hidden-xs"><span class="done-count">${report.done_count}</span></div>
                                  <div class="col-lg-1 col-md-1 hidden-sm hidden-xs"><a name="file" href="${report.file}"><span class="glyphicon glyphicon-file"></span></a></div>`;
    if (report.status !== 'C') {
        report_template += `<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6"><span class="status ${report.status === 'A' ? 'accepted' : 'rejected'}">${report.status === 'A' ? 'Принято' : 'Отклонено'}</span></div>`;
    } else {
        report_template += `<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <button id="rep_${report.id}_accept" class="btn btn-primary btn-accept" name="accept">Принять</button>
                                <button id="rep_${report.id}_reject" class="btn btn-primary btn-reject" name="reject">Отклонить</button>
                            </div>`;
    }
    report_template += '</div>';

    $('#reports_container').append(report_template);

    set_report_modal_link_click_handler(report.id);
    $(`#rep_${report.id}_accept`).click(function () {
        manage_report('accept', report.id);
    });
    $(`#rep_${report.id}_reject`).click(function () {
        manage_report('reject', report.id);
    });

    let file_field = $(`#rep${report.id} a[name="file"]`);
    if (file_field.attr('href') === '') file_field.click(function (e) {
        e.preventDefault();
    }); else file_field.unbind('click');
}

function set_report_modal_link_click_handler(report_id) {
     $(`#report_modal_link_${report_id}`).click(function () {
         $('#report-window #report_id').val(report_id);
         load_modal_report();
         $('#report-window').modal('show');
     });
}

function manage_report(action, report_id) {
    const csrftoken = $('.reports input[name="csrfmiddlewaretoken"]').val();

    $.ajax({
        method: 'post',
        url: `/internal_api/reports/${report_id}`,
        data: {
            decision: action
        },
        headers: {
            'X-CSRFToken': csrftoken
        },
        success: function (data) {
            reload_reports(true);
        }
    });
}