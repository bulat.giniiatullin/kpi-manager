$(document).ready(function () {
    $.ajax({
       method: 'GET',
       url: '/internal_api/departments/under_control',
       dataType: 'json',
       success: function (data) {
           show_departments(data['departments']);
       } 
    });

    $('#set-creating-window #save').click(send_set_creation_form);
});

function show_departments(departments) {
    departments.forEach(show_department_option)
}

function show_department_option(department) {
    $('#set-creating-window #department').append(`<option value="${department.id}">${department.name}</option>`)
}

function send_set_creation_form() {
    const csrftoken = $('#set-creating-window input[name="csrfmiddlewaretoken"]').val();
    let nameField = $('#set-creating-window #name');
    let departmentIdField = $('#set-creating-window #department');
    let fromField = $('#set-creating-window #from');
    let toField = $('#set-creating-window #to');

    $.ajax({
        method: 'POST',
        url: '/internal_api/sets',
        data: {
            'name': nameField.val(),
            'department_id': departmentIdField.val(),
            'date_from': fromField.val(),
            'date_to': toField.val()
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });

    $('#set-creating-window').modal('hide');
    nameField.val('');
    fromField.val('');
    toField.val('');
}