let user_photo;

$(document).ready(function () {
    $('#save_employee_data').click(update_profile_data);
    $('#save_auth_data').click(update_auth_data);

    $('.profile-data-form #photo-field').change(read_user_photo);
});

function update_auth_data() {
    const login = $('.settings #login').val();
    const password = $('.settings #password').val();
    const password_repeat = $('.settings #password_repeat').val();
    const csrftoken = $('#auth-data-form input[name="csrfmiddlewaretoken"]').val();

    $('.auth-data-form .alert').hide();

    if (password === password_repeat) {
        $.ajax({
            method: 'post',
            url: '/internal_api/users/update',
            data: {
                login: login,
                password: password
            },
            headers: {
                'X-CSRFToken': csrftoken
            },
            success: function (response) {
                $('.auth-data-form #auth_success_alert').show();
            },
            error: function (response) {
                if (response.status === 400) {
                    let error_code = response.getResponseHeader('error_code');
                    if (error_code === 'password.short') $('.auth-data-form #password_short_alert').show();
                    else if (error_code === 'login.invalid') $('.auth-data-form #login_invalid_alert').show();
                } else {
                    $('.auth-data-form #unknown_error_alert').show();
                }
            }
        });
    } else {
        $('.auth-data-form #password_repeat_alert').show();
    }
}

function update_profile_data() {
    const name = $('.settings #name').val();
    const birth_date = $('.settings #birth-date').val();
    const email = $('.settings #email').val();
    const csrftoken = $('#profile-data-form input[name="csrfmiddlewaretoken"]').val();

    $('.profile-data-form .alert').hide();

    let post_data = new FormData();
    post_data.append('name', name);
    post_data.append('birth-date', birth_date);
    post_data.append('email', email);
    post_data.append('user-photo', user_photo);

    $.ajax({
        method: 'post',
        url: '/internal_api/users/update',
        data: post_data,
        processData: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        headers: {
            'X-CSRFToken': csrftoken,
        },
        success: function (response) {
            $('.profile-data-form #profile_success_alert').show();
        },
        error: function (response) {
            if (response.status === 400) {
                let error_code = response.getResponseHeader('error_code');
                if (error_code === 'email.invalid') $('.profile-data-form #invalid_email_alert').show();
            } else {
                $('.profile-data-form #unknown_error_alert').show();
            }
        }
    });
}

function read_user_photo() {
    let photo = $(this).prop('files')[0];
    user_photo = photo;

    let reader = new FileReader();
    reader.onload = function (e) {
        show_photo(photo, e.target.result)
    };

    reader.readAsDataURL(photo);
}

function show_photo(photo, reader_result) {
    $('.profile-data-form #user-photo')
        .attr('src', reader_result)
        .attr('title', photo.name);
}