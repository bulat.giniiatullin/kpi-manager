$(document).ready(function () {
    reload_sets();

    $('#open-set-creating-modal-btn').click(function () {
        $('#set-creating-window').modal('show');
    });

    $('#filters #filter-btn').click(reload_sets);
    $(document).ajaxComplete(reload_sets);

    $('#open-sets-header > a[href="#open-sets"]').click(function () {
        if ($(this).attr('aria-expanded') === 'false') {
            $('#open-sets-header > span.glyphicon').removeClass('glyphicon-menu-up glyphicon-menu-down').addClass('glyphicon-menu-up');
        } else {
            $('#open-sets-header > span.glyphicon').removeClass('glyphicon-menu-up glyphicon-menu-down').addClass('glyphicon-menu-down');
        }
    });

    $('#closed-sets-header > a[href="#closed-sets"]').click(function () {
        if ($(this).attr('aria-expanded') === 'false') {
            $('#closed-sets-header > span.glyphicon').removeClass('glyphicon-menu-up glyphicon-menu-down').addClass('glyphicon-menu-up');
        } else {
            $('#closed-sets-header > span.glyphicon').removeClass('glyphicon-menu-up glyphicon-menu-down').addClass('glyphicon-menu-down');
        }
    });
});

function reload_sets() {
    let name_filter = $('#filters #indicator_name').val();
    let data = {};
    if (name_filter !== '') data.name = name_filter;

    load_list_of_sets(data, 'uncompleted');
    load_list_of_sets(data, 'completed');
}

function load_list_of_sets(params, is_completed) {
    params.completed = is_completed;
    $.ajax({
        async: false,
        method: 'GET',
        url: '/internal_api/sets',
        global: false,
        data: params,
        dataType: 'json',
        success: function (response_data) {
            show_sets(params.completed, response_data['set_list']);
        }
    });
}

function show_sets(is_completed, sets) {
    let parent;
    if (is_completed === 'uncompleted') {
        parent = $('#open-sets');
    } else {
        parent = $('#closed-sets');
    }
    parent.empty();
    sets.forEach(function (set) {
        show_one_set(set, parent);
    });
}

function show_one_set(set, parent) {
    let set_template = `<a href="/indicators/sets/${set.id}">
                        <div id="set_${set.id}" class="row set">
                            <div class="set-name col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                ${set.name}
                            </div>
                            <div class="set-date col-lg-4 col-md-4 col-sm-4 hidden-xs">
                                ${set.start_date} -- ${set.end_date}
                            </div>
                            <div class="set-department col-lg-4 col-md-4 col-sm-4 hidden-xs">
                                ${set.department_name}
                            </div>
                        </div>
                        </a>`;
    parent.append(set_template);
}