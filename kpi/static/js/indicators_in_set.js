const ON_ONE_PAGE = 10;
let current_page = 1;
let set_id;

$(document).ready(function () {
    set_id = $('#set_id').val();

    reload_indicators(true);

    $('#filters #filter-btn').click(function () {
        reload_indicators(true);
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() === $(document).height()) {
            reload_indicators(false);
        }
    });
});

function reload_indicators(from_first_page = false) {
    $('#loading_icon').css({'display': 'block'});

    let params = prepare_params();

    params.pageing_by = ON_ONE_PAGE;

    if (from_first_page) current_page = 1;
    else current_page += 1;

    params.page = current_page;

    $.ajax({
        async: false,
        method: 'GET',
        url: `/internal_api/sets/${set_id}/indicators`,
        data: params,
        dataType: 'json',
        success: function (data) {
            show_indicators(data['indicator_list'], from_first_page);
            $('#loading_icon').css('display', 'none');
            if (from_first_page) update_total_progress_bar();
        }
    });
}

function prepare_params() {
    let name_filter_value = $('#filters #indicator_name').val();
    let emp_fio_filter = $('#filters #emp_fio option:selected');
    let is_completed_checked = $('#filters #completed_checkbox').is(':checked');
    let is_uncompleted_checked = $('#filters #uncompleted_checkbox').is(':checked');

    let data = {};
    if (name_filter_value !== '') data.name = name_filter_value;
    if (emp_fio_filter.val() !== '-1') data.employee_name = emp_fio_filter.text();
    if (is_completed_checked && !is_uncompleted_checked) data.completed = 'completed';
    else if (!is_completed_checked && is_uncompleted_checked) data.completed = 'uncompleted';
    else data.completed = 'all';

    return data;
}

function show_indicators(indicators, from_first_page = false) {
    if (from_first_page) $('#indicators_container').empty();
    indicators.forEach(show_indicator);
}

function show_indicator(indicator) {
    let indicator_template = `<div id="ind${indicator.id}" class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h3 id="indicator_name" class="list-group-item-heading">${indicator.name}</h3>
                                        <p id="set_name">${indicator.set_name}</p>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align="right">
                                        <b>Срок выполнения:</b> <span id="deadline">${indicator.deadline}</span>
                                        <div class="progress" style="margin-bottom:0;">
                                            <div class="progress-bar" role="progressbar" style="width: ${indicator.done_count / indicator.count * 100}%;" aria-valuenow="${indicator.done_count / indicator.count * 100}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <b>Выполнено:</b> <span id="done_count">${indicator.done_count}</span> <b>Всего:</b> <span id="all_count">${indicator.count}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <p id="description" class="list-group-item-text">${indicator.description}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div align="right"><a href="/indicators/distribute/${indicator.id}" role ="button" class="btn btn-primary">Распределить</a></div>
                                    </div>
                                </div>
                                <div class="collapse-info">
                                    <a id="distribution_list_btn_${indicator.id}" href="#distribution_list_${indicator.id}" data-toggle="collapse" style="margin-right: 5rem">Список распределения</a>
                                    <a href="/indicators/reports/${indicator.id}">Рассмотреть отчёты</a>
                                    <div id="distribution_list_${indicator.id}" class="collapse">
                                        <table class="table table-striped">
                                          <tbody>
                                          </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>`;
    $('#indicators_container').append(indicator_template);
    $(`#distribution_list_btn_${indicator.id}`).click(function () {
        load_distribution_list(indicator.id)
    })
}

function load_distribution_list(indicator_id) {
    $.ajax({
        method: 'get',
        url: `/internal_api/indicators/${indicator_id}/distribution_list`,
        dataType: 'json',
        success: function (data) {
            show_distribution_list(data['distribution_list'], indicator_id);
        }
    });
}

function show_distribution_list(distribution_list, indicator_id) {
    let parent = $(`#distribution_list_${indicator_id} > table > tbody`);
    parent.empty();
    if (distribution_list.length === 0 || distribution_list === undefined) {
        parent.append('<p>Показатель ещё не распределен</p>');
    } else {
        distribution_list.forEach(function (element) {
            show_element_of_distribution_list(element, parent);
        });
    }
}

function show_element_of_distribution_list(dl_element, container) {
    let element = `<tr>
                      <td>
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                ${dl_element.employee_fio !== undefined ? dl_element.employee_fio : dl_element.department_name}
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                ${dl_element.done_count} из ${dl_element.count}
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <div class="progress" style="margin-bottom:0;">
                                    <div class="progress-bar" role="progressbar" style="width: ${dl_element.done_count / dl_element.count * 100}%;" aria-valuenow="${dl_element.done_count / dl_element.count * 100}" aria-valuemin="0" aria-valuemax="100">${dl_element.done_count / dl_element.count * 100}%</div>
                                </div>
                            </div>
                        </div>
                      </td>
                    </tr>`;
    container.append(element);
}

function update_total_progress_bar() {
    $.ajax({
        method: 'get',
        url: `/internal_api/sets/${set_id}`,
        dataType: 'json',
        success: function (data) {
            let set_data = data['set'];
            let percents = (set_data.total_done_count / set_data.total_count * 100).toFixed(1);
            console.log(percents);
            $('#total_progress_bar').css({'width': `${percents}%`})
                .text(`${percents}%`)
                .attr('aria-valuenow', `${percents}`);
        }
    });
}
