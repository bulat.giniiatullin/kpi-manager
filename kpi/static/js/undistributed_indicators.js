const ON_ONE_PAGE = 10;
let current_page = 1;

$(document).ready(function () {
    reload_indicators(true);

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() === $(document).height()) {
            reload_indicators(false);
        }
    });
});

function reload_indicators(from_first_page = false) {
    $('#loading_icon').css({'display': 'block'});

    let params = {};

    params.pageing_by = ON_ONE_PAGE;

    if (from_first_page) current_page = 1;
    else current_page += 1;

    params.page = current_page;

    $.ajax({
        async: false,
        method: 'GET',
        url: '/internal_api/indicators/undistributed',
        data: params,
        dataType: 'json',
        success: function (data) {
            show_indicators(data['indicators'], from_first_page);
            $('#loading_icon').css('display', 'none');
        }
    });
}

function show_indicators(indicators, from_first_page = false) {
    if (from_first_page) $('#indicators_container').empty();
    indicators.forEach(show_indicator);
}

function show_indicator(indicator) {
    let indicator_template = `<div class="list-group-item" id="ind${indicator.id}">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <h3 id="indicator_name" class="list-group-item-heading">${indicator.name}</h3>
                                        <p id="set_name">${indicator.set_name}</p>
                                        <p><b>Необходимо выполнить:</b> <span id="todo_count">${indicator.count}</span></p>
                                        <p id="description" class="list-group-item-text">${indicator.description}</p>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" align="right">
                                        <b>Срок выполнения:</b> <span id="deadline">${indicator.deadline}</span>
                                        <a id="self_distribute" role ="button" class="btn btn-primary self_distribute">Делегировать себе</a>
                                        <a href="/indicators/distribute/${indicator.id}" role ="button" class="btn btn-primary">Распределить</a>
                                    </div>
                                </div>
                              </div>`;
    $('#indicators_container').append(indicator_template);
    $(`#ind${indicator.id} .self_distribute`).click(function () {
        self_distribute(indicator.id, indicator.count);
    });
}

function self_distribute(indicator_id, todo_count) {
    $.ajax({
        method: 'post',
        url: '/internal_api/indicators/self_distribute',
        data: {
            indicator_id: indicator_id,
            todo_count: todo_count
        },
        success: function (request) {
            reload_indicators(true);
        }
    });
}