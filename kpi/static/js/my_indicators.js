const ON_ONE_PAGE = 10;
let current_page = 1;

$(document).ready(function () {
    reload_indicators(true);

    $('#filters #filter-btn').click(function () {
        reload_indicators(true);
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() === $(document).height()) {
            reload_indicators(false);
        }
    });

    $('a.reports_collapse_link').click(function () {
        if ($(this).is('.collapsed')) {
            let id = $(this).attr('href').slice(9);
            load_reports_for_indicator(id);
        }
    });
});

function reload_indicators(from_first_page = false) {
    $('#loading_icon').css({'display': 'block'});

    let params = prepare_params();

    params.pageing_by = ON_ONE_PAGE;

    if (from_first_page) current_page = 1;
    else current_page += 1;

    params.page = current_page;

    $.ajax({
        async: false,
        method: 'GET',
        url: '/internal_api/indicators',
        data: params,
        dataType: 'json',
        success: function (data) {
            show_indicators(data['indicators'], from_first_page);
            $('#loading_icon').css('display', 'none');
        }
    });
}

function prepare_params() {
    let name_filter_value = $('#filters #indicator_name').val();
    let date_from_filter_value = $('#filters #date_from').val();
    let date_to_filter_value = $('#filters #date_to').val();
    let set_name_filter = $('#filters #set_name option:selected');
    let is_completed_checked = $('#filters #completed_checkbox').is(':checked');
    let is_uncompleted_checked = $('#filters #uncompleted_checkbox').is(':checked');

    let data = {};
    if (name_filter_value !== '') data.name = name_filter_value;
    if (date_from_filter_value !== '') data.date_from = date_from_filter_value;
    if (date_to_filter_value !== '') data.date_to = date_to_filter_value;
    if (set_name_filter.val() !== '-1') data.set = set_name_filter.text();
    if (is_completed_checked && !is_uncompleted_checked) data.completed = 'completed';
    else if (!is_completed_checked && is_uncompleted_checked) data.completed = 'uncompleted';
    else data.completed = 'all';

    return data;
}

function show_indicators(indicators, from_first_page = false) {
    if (from_first_page) $('#indicators_container').empty();
    indicators.forEach(show_indicator);
}

function show_indicator(indicator) {
    let indicator_template = `<div id="indicator_${indicator.indicator_id}" class="list-group-item" >
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="list-group-item-heading" id="indicator_name">${indicator.name}</h3>
                                        <p id="set_name">${indicator.set_name}</p>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align="right">
                                        <b>Срок выполнения:</b> <span id="deadline">${indicator.deadline}</span>
                                        <div class="progress" style="margin-bottom:0;">
                                            <div id="progress" class="progress-bar" role="progressbar" style="width: ${indicator.done_count / indicator.count * 100}%;" aria-valuenow="${indicator.done_count / indicator.count * 100}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <b>Выполнено:</b> <span id="done_count">${indicator.done_count}</span> <b>Всего:</b> <span id="all_count">${indicator.count}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                        <p id="description" class="list-group-item-text">${indicator.description}</p>
                            
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <a href="/indicators/send_report/${indicator.id}" role ="button" class="btn btn-primary" >Выполнить</a>
                                    </div>
                                </div>
                                <div class="collapse-info">
                                    <a href="#reports_${indicator.indicator_id}" class="reports_collapse_link collapsed" data-toggle="collapse" aria-expanded="false">Отчёты о выполнении</a>
                                    <div id="reports_${indicator.indicator_id}" class="collapse" aria-expanded="false">
                                        <table class="table table-striped">
                                          <thead>
                                            <tr><td><div class="row">
                                              <div class="col-xs-4 col-sm-3 col-md-3">Название</div>
                                              <div class="hidden-xs col-sm-2 col-md-2">Дата</div>
                                              <div class="col-xs-4 col-sm-2 col-md-2">Выполнено</div>
                                              <div class="hidden-xs col-sm-3 col-md-3">Файл</div>
                                              <div class="col-xs-2 col-sm-2 col-md-2">Статус</div>
                                            </div></td></tr>
                                          </thead>
                                          <tbody id="reports_${indicator.indicator_id}_tbody"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>`;
    $('#indicators_container').append(indicator_template);
}

function load_reports_for_indicator(indicator_id) {
    $.ajax({
        async: true,
        method: 'GET',
        url: '/internal_api/reports',
        data: {
            user: 'current',
            indicator: indicator_id
        },
        dataType: 'json',
        success: function (data) {
            show_reports(data['report_list'], indicator_id);
            set_report_modal_link_click_handler();
        }
    });
}

function show_reports(reports, indicator_id) {
    $(`#reports_${indicator_id}_tbody`).empty();
    reports.forEach(function (report) {
        show_report(report, indicator_id);
    })
}

function show_report(report, indicator_id) {
    $(`#reports_${indicator_id}_tbody`).append(`<tr><td><div class="row" id="rep${report.id}">
                                                  <div class="col-xs-4 col-sm-3 col-md-3"><a id="report_modal_link_${report.id}" class="report_modal_link">${report.name}</a></div>
                                                  <div class="hidden-xs col-sm-2 col-md-2">${report.date}</div>
                                                  <div class="col-xs-4 col-sm-2 col-md-2">${report.done_count}</div>
                                                  <div class="hidden-xs col-sm-3 col-md-3"><a href="${report.file}">Скачать</a></div>
                                                  <div class="col-xs-2 col-sm-2 col-md-2 status">${report.status === 'C' ? 'На рассмотрении' : report.status === 'A' ? 'Принято' : 'Отклонено' }</div>
                                                </div></td></tr>`);
    set_report_modal_link_click_handler(report.id);

    let status_field = $(`#rep${report.id} div.status`);
    status_field.removeClass('accepted rejected');
    if (report.status === 'A') status_field.addClass('accepted');
    else if (report.status === 'R') status_field.addClass('rejected');
}

function set_report_modal_link_click_handler(report_id) {
     $(`#report_modal_link_${report_id}`).click(function () {
         $('#report-window #report_id').val(report_id);
         load_modal_report();
         $('#report-window').modal('show');
     });
}