$(document).ready(function () {
    load_sets();

    $('#add-set').click(function () {
        $('#set-creating-window').modal('show');
    });

    $('form#indicator-creating-form select#department').change(load_sets);
    $(document).ajaxSuccess(load_sets)
});

function load_sets() {
    let departmentId = $('form#indicator-creating-form select#department').val();

    $.ajax({
        method: 'GET',
        url: '/internal_api/sets',
        data: {
            'completed': 'false',
            'department_id': departmentId
        },
        global: false,
        dataType: 'json',
        success: function (data) {
            show_sets(data['set_list']);
        }
    })
}

function show_sets(sets) {
    $('#set_select').empty();
    sets.forEach(show_one_set)
}

function show_one_set(set) {
    $('#set_select').append(`<option value="${set.id}">${set.name}</option>`)
}