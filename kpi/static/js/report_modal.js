$(document).ready(function () {
    $('#report-window #accept').click(function () {
        manage_modal_report('accept');
    });
    $('#report-window #reject').click(function () {
        manage_modal_report('reject');
    })
});

function load_modal_report() {
    let report_id = $('#report-window #report_id').val();

    $.ajax({
        method: 'get',
        url: `/internal_api/reports/${report_id}`,
        dataType: 'json',
        success: function (data) {
            show_report_in_modal(data);
        }
    });
}

function show_report_in_modal(report) {
    $('#report-window #name').empty().append(report.name);
    $('#report-window #date').empty().append(report.date);
    $('#report-window #employee').empty().append(report.employee);
    $('#report-window #description').empty().append(report.description);
    $('#report-window #done_count').empty().append(report.done_count);

    let file = $('#report-window #file');
    file.attr('href', report.file);
    if (file.attr('href') === '') file.click(function (e) {
        e.preventDefault();
    }); else file.unbind('click');

    let status_element = $('#report-window #status');
    status_element
        .empty()
        .append(report.status === 'C' ? 'На рассмотрении' : report.status === 'A' ? 'Принято' : 'Отклонено')
        .removeClass('accepted rejected');
    if (report.status === 'A') status_element.addClass('accepted');
    else if (report.status === 'R') status_element.addClass('rejected');

    if ($('#reports_list_page_btns')) {
        if (report.status === 'C') {
            $('#reports_list_page_btns #ok-btn').hide();
            $('#accept, #reject').show();
        } else {
            $('#reports_list_page_btns #ok-btn').show();
            $('#accept, #reject').hide();
        }
    }
}

function manage_modal_report(action) {
    let report_id = $('#report-window #report_id').val();
    const csrftoken = $('#report-window input[name="csrfmiddlewaretoken"]').val();

    $.ajax({
        method: 'post',
        url: `/internal_api/reports/${report_id}`,
        data: {
            decision: action
        },
        headers: {
            'X-CSRFToken': csrftoken
        },
        success: function (data) {
            load_modal_report();
            reload_reports(true);
        }
    });
}